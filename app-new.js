$('li.add-more-insights').click(function(){
	$('.add-insight-holder').show();
});
$('li.edit-this-insight').click(function(){
	$(this).children('div.edit-insight-holder').show();
});
$('.close').click(function(){
	$(this).parent('div').fadeOut();
});
$("button.close").click(function(){
	$(this).parent().parent().parent().parent().parent('div').fadeOut();
});
$(document).ready(function(){
	if( $(".status-slider > div").hasClass("off")) {
		$(".status-slider p.status-text").text("off");
	} else {
	$(".status-slider p.status-text").text("on");	
	}
});